import React from 'react';
import { Link } from 'react-router-dom';
import ProgressCircle from  '../common/ProgressCircle';
import store from '../../store';

import './StageList.sass';

class InventoryList extends React.Component {
	constructor(props) {
		super(props);
		this.state =	{
			course: this.props.course,
			percentage: [50, 0],
		};
	}

	componentDidMount(){
		store.getState().auth.user.accountStats.courses.map((d, i) => {
			if( d.slug === this.state.courseSlug ){
				this.setState({watchHistory: d.stages, loaded: true});
			}
			let percentage = [];
			d.stages.forEach(function(el, i){
				let w = 0, t = 0;
				el.levels.forEach(function(cl, j){
					t++;
					if( cl === 'watched') w++;
				});
				percentage = percentage.concat([Math.round(w/t*100)]);
			});
			this.setState({ percentage: percentage });
		});
	}

	render() {
    if ( !this.props.course ) {
      return (
        <div className="stage-items">
        </div>
      )
    } else {
      return (
        <div className="stage-items">
          {
            this.state.course.stages.map( (d, i) => {
              return (
                <Link
                  key={i}
                  to={"/course/" + this.state.course.slug + "/" + (i+1) +"/"}
                  className="stage-item-single">
                  <div className="stage-no">stage {i+1}</div>
                  <div className="stage-thumbnail"><img src={"/src/assets/images/stage" + (i+1) + ".jpg"} alt="Stage 1" /></div>
                  <ProgressCircle data={this.state.percentage[i]} />
                </Link>
              )
            })
          }
        </div>
      )
    }
	}
}

export default InventoryList;
