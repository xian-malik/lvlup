import React from 'react';
import axios from 'axios';
import store from '../../../store';

import './AccountForm.sass';

class AccountForm extends React.Component {
	constructor(props) {
		super(props);
		this.state =	{
			fi_avatar: null,
			fi_avatar_preview: null,
			fi_username: '',
			fi_email: '',
			fi_password: '',
			fi_dob_dd: '1',
			fi_dob_mm: '1',
			fi_dob_yyyy: '1970',
			fi_countrycode: '',
			fi_phone: '',
			fi_firstName: '',
			fi_lastName: '',
			loaded: false
		};
		this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleImageChange = this.handleImageChange.bind(this);
  }

	componentDidMount(){
		const user = store.getState().auth.user,
					dob = new Date(user.dob);
		this.setState({
			fi_username: user.username,
			fi_email: user.email,
			fi_dob_dd: dob.getDate(),
			fi_dob_mm: dob.getMonth(),
			fi_dob_yyyy: dob.getFullYear(),
			fi_countrycode: user.countrycode,
			fi_phone: user.phone,
			fi_firstName: user.name,
			fi_lastName: '',
			loaded: true,
			button: 'EDIT',
		});
	}

  handleChange (evt) {
    this.setState({ [evt.target.name]: evt.target.value });
  }

  handleImageChange (evt) {
		const url = URL.createObjectURL(evt.target.files[0]);
    this.setState({
			fi_avatar: evt.target.files[0],
			fi_avatar_preview: url
		});
		console.log(url);
  }

	handleSubmit (evt) {
		evt.preventDefault();
		if(this.state.button === 'EDIT') {
			this.setState({button: 'SAVE'});
		} else {
			let userData = {};
			const user = store.getState().auth.user,
						dob = new Date(user.dob);
	    userData.username = this.state.fi_username;
		  if( this.state.fi_name != user.name ) {
		    userData.name = this.state.fi_name;
		  }
		  if( this.state.fi_email != user.email ) {
		    userData.email = this.state.fi_email;
		  }
		  if( this.state.fi_countrycode != user.countrycode ) {
		    userData.countrycode = this.state.fi_countrycode;
		  }
		  if( this.state.fi_phone != user.phone ) {
		    userData.phone = this.state.fi_phone;
		  }
		  if( this.state.fi_dob_dd != dob.getDate() && this.state.fi_dob_mm != dob.getMonth() && this.state.fi_dob_yyyy != dob.getFullYear() ) {
		    userData.dob = new Date(this.state.fi_dob_yyyy, this.state.fi_dob_mm-1, this.state.fi_dob_dd);
		  }
		  if( this.state.fi_password != null ) {
		    userData.password = this.state.fi_password;
			}
		  axios
		    .post('/api/users/edit', userData, { 'Content-Type': 'application/x-www-form-urlencoded' })
		    .then(res => {
		      console.log('Ok')
		    })
		    .catch(err =>{
		      console.log('error')
		    });
			this.setState({button: 'EDIT'});
		}
	}

	render() {
		if (!this.state.loaded) {
			return (
				<div>Loading</div>
			);
		} else {
	    let Days = [], Months = [], Years = [];
	    for (var i = 1; i <= 31 ; i++) { Days.push(<option key={i} value={i}>{i}</option>) };
	    for (var i = 1; i <= 12 ; i++) { Months.push(<option key={i} value={i}>{i}</option>) };
	    for (var i = 1970; i <= 2020 ; i++) { Years.push(<option key={i} value={i}>{i}</option>) };

			return (
	      <form id="EditProfileForm" className="center-form edit-form" onSubmit={ this.handleSubmit }>
	        <div className="input-file">
	          <label htmlFor="fi_avatar">
							<img src={ (this.state.fi_avatar_preview != null) ? (this.state.fi_avatar_preview) : ("./src/assets/avatars/profilePicture.png") } alt="" />
						</label>
	          <input type="file" name="fi_avatar" id="fi_avatar" onChange={ (evt) => this.handleImageChange(evt) } accept="image/*" />
	        </div>
	        <div className="input-header">
	          <p>PRIVATE ACCOUNT DETAILS</p>
	        </div>
	        <div className="input-group">
	          <label htmlFor="fi_email">EMAIL</label>
	          <input type="email" name="fi_email" id="fi_email" value={this.state.fi_email} onChange={ this.handleChange } />
	        </div>
	        <div className="input-group">
	          <label htmlFor="fi_username">USERNAME</label>
	          <input type="text" name="fi_username" id="fi_username" value={this.state.fi_username} onChange={ this.handleChange } disabled/>
	        </div>
	        <div className="input-group">
	          <label htmlFor="fi_password">PASSWORD</label>
	          <input type="password" name="fi_password" id="fi_password" value={this.state.fi_password} onChange={ this.handleChange } />
	        </div>
	        <div className="input-group">
	          <label htmlFor="fi_dob">DATE OF BIRTH</label>
	          <select name="fi_dob_dd" id="fi_dob_dd" value={this.state.fi_dob_dd} onChange={ this.handleChange } >
	            {Days}
	          </select>
	          <select name="fi_dob_mm" id="fi_dob_mm" value={this.state.fi_dob_mm} onChange={ this.handleChange } >
	            {Months}
	          </select>
	          <select name="fi_dob_yyyy" id="fi_dob_yyyy" value={this.state.fi_dob_yyyy} onChange={ this.handleChange } >
	            {Years}
	          </select>
	        </div>
	        <div className="input-group">
	          <label htmlFor="fi_countrycode">COUNTRY CODE</label>
	          <input type="text" name="fi_countrycode" id="fi_countrycode" value={this.state.fi_countrycode} onChange={ this.handleChange } />
	        </div>
	        <div className="input-group">
	          <label htmlFor="fi_phone">PHONE</label>
	          <input type="tel" name="fi_phone" id="fi_phone" value={this.state.fi_phone} onChange={ this.handleChange } />
	        </div>
	        <div className="input-group">
	          <label htmlFor="fi_firstName">FIRST NAME</label>
	          <input type="text" name="fi_firstName" id="fi_firstName" value={this.state.fi_firstName} onChange={ this.handleChange } />
	        </div>
	        <div className="input-group">
	          <label htmlFor="fi_lastName">LAST NAME</label>
	          <input type="text" name="fi_lastName" id="fi_lastName" value={this.state.fi_lastName} onChange={ this.handleChange } />
	        </div>
	        <div className={ "input-group submit-" + this.state.button.toLowerCase() }>
	          <input type="submit" name="fi_submit" value={this.state.button} />
	        </div>
	      </form>
			)
		}
	}
}

export default AccountForm;
