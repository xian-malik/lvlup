import React from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { registerUser } from '../../../Redux/actions/authActions';
import { withRouter } from 'react-router-dom';

import passport from 'passport';

import './LoginForm.sass';

class RegisterForm extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			registerName: '',
			registerEmail: '',
			registerUsername: '',
			registerPassword: '',
			response: '',
			errors: ''
		}
    this.handleChange = this.handleChange.bind(this);
    this.handleRegister = this.handleRegister.bind(this);
  }

  handleChange(evt) {
    this.setState({ [evt.target.name]: evt.target.value });
	}
	handleRegister(evt) {
		evt.preventDefault();

		const userData = {
			name: this.state.registerName,
			email: this.state.registerEmail,
			username: this.state.registerUsername,
			password: this.state.registerPassword
		}
	  axios
	    .post('/api/users/register', userData, { 'Content-Type': 'application/x-www-form-urlencoded' })
	    .then(res => {
	      this.setState({response: 'Successfully registered.'})
	    })
	    .catch(err =>{
	      this.setState({errors: err})
	    });
	}

  componentWillReceiveProps(nextProps) {
  }

	render() {
		if ( this.state.response === ''){
			return (
				<form id="LoginForm" onSubmit={ this.handleRegister }>
					<div className="text-center">
						<input
							type="text"
							placeholder="Full Name"
							className="input-field"
							name="registerName"
							autoComplete="off"
							value={this.state.registerName}
							onChange={ this.handleChange } />
						<input
							type="email"
							placeholder="Email"
							className="input-field"
							name="registerEmail"
							autoComplete="off"
							value={this.state.registerEmail}
							onChange={ this.handleChange } />
						<input
							type="text"
							placeholder="Username"
							className="input-field"
							name="registerUsername"
							autoComplete="off"
							value={this.state.registerUsername}
							onChange={ this.handleChange } />
						<input
							type="password"
							placeholder="Password"
							className="input-field"
							name="registerPassword"
							autoComplete="off"
							value={this.state.registerPassword}
							onChange={ this.handleChange } />
					</div>
					<div className="text-center">
						<input type="submit" className="button" value="REGISTER" />
					</div>
					<div className="form-response">
						{this.state.error}
					</div>
				</form>
			)
		} else {
			return (
				<div className="form-response form-success text-center">
					{this.state.response}
				</div>
			)
		}
	}
}

RegisterForm.propTypes = {
	registerUser: PropTypes.func.isRequired,
	auth: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
	auth: state.auth,
})
export default withRouter(connect(mapStateToProps, {registerUser})(RegisterForm));
