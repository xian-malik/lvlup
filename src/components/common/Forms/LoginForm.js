import React from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { loginUser } from '../../../Redux/actions/authActions';
import { withRouter } from 'react-router-dom'

import passport from 'passport';

import './LoginForm.sass';

class LoginForm extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			loginUsername: '',
			loginPassword: '',
			response: '',
			errors: ''
		}
    this.handleChange = this.handleChange.bind(this);
    this.handleLogin = this.handleLogin.bind(this);
  }

  handleChange(evt) {
    this.setState({ [evt.target.name]: evt.target.value });
	}
	handleLogin(evt) {
		evt.preventDefault();

		const userData = {
			username: this.state.loginUsername,
			password: this.state.loginPassword
		}
		this.props.loginUser(userData);
	}

  componentWillReceiveProps(nextProps) {
    if (nextProps.auth.isAuthenticated) {
      this.props.history.push('/myaccount');
    }

    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }

	render() {
		return (
			<form id="LoginForm" onSubmit={ this.handleLogin }>
				<div className="text-center">
					<input
						type="text"
						placeholder="Username"
						className="input-field"
						name="loginUsername"
						autoComplete="off"
						value={this.state.loginUsername}
						onChange={ this.handleChange } />
					<input
						type="password"
						placeholder="Password"
						className="input-field"
						name="loginPassword"
						autoComplete="off"
						value={this.state.loginPassword}
						onChange={ this.handleChange } />
				</div>
				<div className="text-center">
					<input type="submit" className="button" value="LOGIN" />
				</div>
				<div className="form-response">
					{this.state.response}
				</div>
				<div className="form-response">
					{this.state.error}
				</div>
			</form>
		)
	}
}

LoginForm.propTypes = {
	loginUser: PropTypes.func.isRequired,
	auth: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
	auth: state.auth,
})
export default withRouter(connect(mapStateToProps, {loginUser})(LoginForm));
