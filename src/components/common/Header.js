import React from 'react';
import { BrowserRouter as Router, Link } from "react-router-dom";

import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { loginUser } from '../../Redux/actions/authActions';
import { withRouter } from 'react-router-dom';

import './Header.sass';

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      logged: this.props.logged,
      username: '',
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.auth.isAuthenticated) {
      this.setState({ logged: true, username: nextProps.auth.user.username });
    } else {
      this.setState({ logged: false });
    }
  }

  render() {
    const v = this.state.logged;
    return (
  		<header>
  			<div className="header-container">
  				<div className="header-left">
  					<ul className="navbar-nav">
  						<li className="navbar-item"><Link to="/" className="navbar-link">Home</Link></li>
  						<li className="navbar-item"><Link to="/thecourse" className="navbar-link">The Course</Link></li>
  						{ (v) ? <li className="navbar-item"><Link to="/about" className="navbar-link">About</Link></li> : '' }
  					</ul>
  				</div>
  				<div className="header-logo">
  					<img src="./src/assets/images/levelUpLogoNEW.svg" alt="LVLUP" />
  				</div>
  				<div className="header-right">
  					<ul className="navbar-nav">
  						{ (!v) ? <li className="navbar-item"><Link to="/about" className="navbar-link">About</Link></li> : '' }
              { (v) ? <li className="navbar-item"><Link to="/myaccount" className="navbar-link">Dashboard</Link></li> : '' }
              { (v) ? <li className="navbar-item"><Link to="/editprofile" className="navbar-link">Profile</Link></li> : '' }
  						<li className="navbar-item"><Link to={"/" +  (v ? 'logout' : 'login')  } className="navbar-link">{ v ? 'logout ( '+this.state.username+' )' : 'login'  }</Link></li>
  					</ul>
  				</div>
  			</div>
  		</header>
    );
  }
}

Header.propTypes = {
  loginUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default connect(mapStateToProps, { loginUser })(Header);
