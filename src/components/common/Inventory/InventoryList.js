import React from 'react';

import './InventoryList.sass';

class InventoryList extends React.Component {
	constructor(props) {
		super(props);
		this.state =	{
			data: this.props.data,
			lvl_title: '',
		};
	}

	render() {
		return (
			<div className="lvl-inventory">
				<p className="inventory-desc small text-center">
					{this.state.lvl_title}
				</p>
				<div className="inventory-items">
					{
						this.state.data.map( (d, i) => {
							let url = d.replace(/(?:^\w|[A-Z]|\b\w)/g, function(word, index) {
					      return index == 0 ? word.toLowerCase() : word.toUpperCase();
					    }).replace(/\s+/g, '')
							return (
								<div
									key={i}
									className="inventory-item-single"
									onMouseEnter={() => this.setState({lvl_title: d})}
									onMouseLeave={() => this.setState({lvl_title: "LEVEL UP COURSE"})}>
					        <div className="ii-name">{d}</div>
					        <div className="ii-background"><img src={"src/assets/images/ii-" + url + "Parts.png"} alt="Inventory Course" /></div>
					      </div>
							)
						})
					}
				</div>
			</div>
		)
	}
}

export default InventoryList;
