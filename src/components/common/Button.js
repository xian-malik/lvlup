import React from 'react';
import './Button.sass';
import { Link } from "react-router-dom";

class Button extends React.Component {
	constructor(props) {
    super(props);
		this.state =  {
			type: (this.props.type !== undefined )?(this.props.type):'',
			link: (this.props.link !== undefined )?(this.props.link):'#'
		};
  }
	render() {
		return (
			<Link to={this.state.link} className={"button " + this.state.type }>{this.props.text}</Link>
		)
	}
}

export default Button;
