import React from 'react';
import './ProgressCircle.sass';

class ProgressCircle extends React.Component {
	constructor(props) {
    super(props);
  }
	render() {
    let perc = 377 * (100 - this.props.data) / 100;
		return (
      <div className="stage-progress">
        <svg xmlns="http://www.w3.org/2000/svg" width="128" height="128" viewBox="0 0 128 128">
        	<circle cx="64" cy="64" r="60" className="stroke-bg" />
        	<circle cx="64" cy="64" r="60" className="stroke" style={{strokeDashoffset: perc}} />
        </svg>
        <div className="stage-desc">
          <div className="stage-perc">{this.props.data}%</div>
          <div className="stage-perc-desc">COMPLETE</div>
        </div>
      </div>
		)
	}
}

export default ProgressCircle;
