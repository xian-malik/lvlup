import React from 'react';
import UnlockItem from './UnlockItem';

import './Unlocks.sass';

class Unlocks extends React.Component {
	constructor(props) {
		super(props);
		this.state =	{
			data: this.props.data
		};
	}

	render() {
		const UnlockItems = this.state.data.map( (d, i) => {
			return (
				<UnlockItem
					key={i}
					data={d.text}
          locked={d.locked} />
			)
		});
		return (
			<div className="stage-unlocks">
				{UnlockItems}
			</div>
		)
	}
}

export default Unlocks;
