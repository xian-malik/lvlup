import React from 'react';

class UnlockItem extends React.Component {
  constructor(props) {
		super(props);
    this.state = {
      locked: this.props.locked ? 'locked' : 'unlocked',
    }
  }
	render() {
		return (
      <div className={"unlock-single-item " + this.state.locked } >
        <div className="ul-name">{this.props.data}</div>
        <div className="ul-background"><img src={"./src/assets/images/" + this.props.data + ".png"} alt="Course" /></div>
      </div>
		)
	}
}

export default UnlockItem;
