import React from 'react';
import './Button.sass';

class CarouselItem extends React.Component {
	render() {
		return (
      <div className="carousel-item-single">
        <div className="carousel-count"><span>{this.props.count}</span></div>
        <div className="carousel-desc">
          <h3 className="carousel-head">{this.props.head}</h3>
          <p className="carousel-text">{this.props.text}</p>
        </div>
      </div>
		)
	}
}

export default CarouselItem;
