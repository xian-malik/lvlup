import React from 'react';
import './Button.sass';

import CarouselItem from './CarouselItem';

import '../../Scripts/owl.carousel.min.css';
import '../../Scripts/owl.theme.default.min.css';
import '../../Scripts/owl.carousel.min.js';

import './Carousel.sass';

class Carousel extends React.Component {
	constructor(props) {
		super(props);
		this.state =	{
			data: this.props.data
		};
	}

	componentDidMount() {
		$(".owl-carousel").owlCarousel({
			loop: true,
			nav: true,
			center: true,
			items: 1,
		});
	}

	render() {
		const CarouselItems = this.state.data.map( (d, i) => {
			return (
				<CarouselItem
					key={i}
					count={i+1}
					head={d.head}
					text={d.text} />
			)
		});
		return (
			<div className={"carousel owl-carousel owl-theme " + this.props.name + "-carousel"}>
				{CarouselItems}
			</div>
		)
	}
}

export default Carousel;
