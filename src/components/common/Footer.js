import React from 'react';
import { BrowserRouter as Router, Link } from "react-router-dom";

import './Footer.sass';

class Footer extends React.Component {
	render() {
		return (
	    <footer className="footer" id="footer">
	      <div className="footer-container container">
	        <div className="row">
	          <div className="col12 text-center">
	            <ul className="footer-socials">
	              <li><Link to="#"><img src="./src/assets/images/social-youtube.png" /></Link></li>
	              <li><Link to="#"><img src="./src/assets/images/social-instagram.png" /></Link></li>
	              <li><Link to="#"><img src="./src/assets/images/social-twitch.png" /></Link></li>
	              <li><Link to="#"><img src="./src/assets/images/social-twitter.png" /></Link></li>
	              <li><Link to="#"><img src="./src/assets/images/social-facebook.png" /></Link></li>
	            </ul>
	          </div>
	        </div>
	        <div className="row row-center">
	          <div className="col3">
	            <div className="row">
	              <div className="col7">
	                <ul className="footer-nav">
	                  <li className="footer-navhead">Navigation</li>
	                  <li><Link to="/">Home</Link></li>
	                  <li><Link to="/course">The Course</Link></li>
	                  <li><Link to="/about">About</Link></li>
	                  <li><Link to="/login">Login</Link></li>
	                </ul>
	              </div>
	              <div className="col5">
	                <ul className="footer-nav">
	                  <li className="footer-navhead">Info</li>
	                  <li><Link to="#">Contact</Link></li>
	                  <li><Link to="#">Terms</Link></li>
	                  <li><Link to="#">Privacy</Link></li>
	                </ul>
	              </div>
	            </div>
	          </div>
	          <div className="col1">
	            <img src="./src/assets/images/levelUpLogoNEW.svg" alt="Level Up" className="footer-logo" />
	          </div>
	        </div>
	      </div>
	    </footer>
		)
	}
}

export default Footer;
