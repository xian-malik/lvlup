import React from 'react';
import './Grid.sass';

export class Container extends React.Component {
	render() { return ( <div className="container">{this.props.children}</div> ) }
}

export class Row extends React.Component {
	constructor(props) {
    super(props);
		this.state =  {type: (this.props.type !== undefined )?(" row-"+this.props.type):""};
  }
	render() { return ( <div className={"row" + this.state.type}>{this.props.children}</div> ) }
}
export class Col extends React.Component {
	constructor(props) {
    super(props);
		this.state =  {
			md: this.props.md,
			offset: (this.props.offset !== undefined )?(" col-offset"+this.props.offset):"",
			data: (this.props.data !== undefined )?(" "+this.props.data):""
		}
  }
	render() {
		let { md, offset, data } = this.state;
		return (
			<div
				className={"col" + md + offset + data }>
				{this.props.children}
			</div>
		)
	}
}
