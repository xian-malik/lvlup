import React from 'react';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router-dom';

import './InventoryList.sass';

class InventoryList extends React.Component {
	constructor(props) {
		super(props);
		this.state =	{
			data: this.props.data,
			lvl_title: '',
		};
	}

	render() {
		return (
			<div className="lvl-inventory">
				<p className="inventory-desc small text-center">
					{this.state.lvl_title}
				</p>
				<div className="inventory-items">
					{
						this.state.data.map( (d, i) => {
							let imgUrl = d.replace(/(?:^\w|[A-Z]|\b\w)/g, function(word, index) {
					      return index == 0 ? word.toLowerCase() : word.toUpperCase();
					    }).replace(/\s+/g, '');
							let url = ("How to setup").replace(/ /g,"-").toLowerCase();
							return (
  							<Link
									key={i}
									to={"/course/" + url}
									className="inventory-item-single"
									onMouseEnter={() => this.setState({lvl_title: d})}>
					        <div className="ii-name">{d}</div>
					        <div className="ii-background"><img src={"./src/assets/images/ii-" + imgUrl + "Parts.png"} alt="Inventory Course" /></div>
					      </Link>
							)
						})
					}
				</div>
			</div>
		)
	}
}

export default withRouter(InventoryList);
