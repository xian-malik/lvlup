import axios from 'axios';

export const getCourse = (slug) => dispatch => {
  axios
    .post('/api/courses/view', slug, { 'Content-Type': 'application/x-www-form-urlencoded' })
    .then(res => {
      const { token } = res.data;
      res.json({ course: res.data });
    })
    .catch(err =>{
    });
}
