import axios from 'axios';
import setAuthToken from '../../utils/setAuthToken';
import jwt_decode from 'jwt-decode';
import { GET_ERRORS, REGISTER_USER, LOGOUT_USER, SET_CURRENT_USER } from './types';

export const loginUser = (userData) => dispatch => {
  axios
    .post('/api/users/login', userData, { 'Content-Type': 'application/x-www-form-urlencoded' })
    .then(res => {
      const { token } = res.data;
      localStorage.setItem('jwtToken', token);
      setAuthToken(token);

      const decoded = jwt_decode(token);
      dispatch(setCurrentUser(decoded));
    })
    .catch(err =>{
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })}
    );
}
export const registerUser = (userData) => dispatch => {  }

export const logoutUser = (userData) => dispatch => {
  localStorage.removeItem('jwtToken');
  dispatch({
    type: LOGOUT_USER,
    payload: {}
  });
}

// Set logged in user
export const setCurrentUser = decoded => {
  return {
    type: SET_CURRENT_USER,
    payload: decoded
  };
};
