import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { logoutUser } from '../Redux/actions/authActions';

class Logout extends React.Component {
	componentDidMount() {
		this.props.logoutUser();
    this.props.history.push('/login');
	}
	render() {
		return (
	    <div className="page-content">
			</div>
		);
	}
}

Logout.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps, { logoutUser })(Logout);
