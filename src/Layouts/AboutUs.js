import React from 'react';

import Goal from '../Containers/About/Goal';
import Experience from '../Containers/About/Experience';
import Motivation from '../Containers/About/Motivation';

class About extends React.Component {
	render() {
		return (
	    <div className="page-content">
				<Goal />
				<Experience />
				<Motivation />
			</div>
		);
	}
}

export default About;
