import React from 'react';

import Inventory from '../Containers/MyAccount/Inventory';
import YourGoals from '../Containers/MyAccount/YourGoals';
import SideBar from '../Containers/MyAccount/SideBar';
import { Container, Row, Col } from '../components/grid/Grid';

class MyAccount extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			goalData: {
				subscriberGoals: {
					current: 46868,
					monthly: 48138,
					yearly: 103193,
				},
				viewGoals: {
					current: 46868,
					monthly: 48138,
					yearly: 103193,
				}
			}
		}
	}
	render() {
		return (
	    <div className="page-content">
				<Container>
					<Row>
						<Col md='7'>
			      	<Inventory />
			      	<YourGoals data={this.state.goalData} />
						</Col>
						<Col md='5'>
							<SideBar />
						</Col>
					</Row>
				</Container>
			</div>
		);
	}
}

export default MyAccount;
