import React from 'react';

import LoginSection from '../Containers/Login/LoginSection';

class Login extends React.Component {
	render() {
		return (
	    <div className="page-content">
	      <LoginSection />
			</div>
		);
	}
}

export default Login;
