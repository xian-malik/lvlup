import React from 'react';

import NextLevel from '../Containers/Coaching/NextLevel';

class Coaching extends React.Component {
	render() {
		return (
	    <div className="page-content">
				<NextLevel />
			</div>
		);
	}
}

export default Coaching;
