import React from 'react';

import HomeSlider from '../Containers/Home/HomeSlider';
import Audience from '../Containers/Home/Audience';
import UpGame from '../Containers/Home/UpGame';
import TrainingVideo from '../Containers/Home/TrainingVideo';

class Home extends React.Component {
	render() {
		return (
	    <div className="page-content">
	      <HomeSlider />
				<Audience />
				<UpGame />
				<TrainingVideo />
			</div>
		);
	}
}

export default Home;
