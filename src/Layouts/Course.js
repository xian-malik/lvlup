import React from 'react';

import VitalTips from '../Containers/Course/VitalTips';
import SevenStages from '../Containers/Course/SevenStages';
import AchievingGoals from '../Containers/Course/AchievingGoals';
import DiscordServer from '../Containers/Course/DiscordServer';

class Course extends React.Component {
	render() {
		return (
	    <div className="page-content">
				<VitalTips />
				<SevenStages />
				<AchievingGoals />
				<DiscordServer />
			</div>
		);
	}
}

export default Course;
