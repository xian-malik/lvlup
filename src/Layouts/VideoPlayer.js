import React from 'react';
import ReactPlayer from 'react-player';
import { Player } from 'video-react';
import { withRouter } from 'react-router-dom';
import axios from 'axios';
import { Container, Row, Col } from '../components/grid/Grid';
import {connect} from 'react-redux';
import store from '../store';

class VideoPlayer extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			courseSlug: this.props.match.params.course,
			stageSlug: this.props.match.params.stage,
			course: {},
			loaded: false,
			watchHistory: [],
			percentage: 0,
			currentVideoUrl: '',
			flag: false,
		}
	  this._isMounted = false;
		this.GeneratedStage = '';
		this.handleLevel = this.handleLevel.bind(this);
		this.generateStage = this.generateStage.bind(this);
		this.simulateClick = this.simulateClick.bind(this);
		this.handleVideoEnd = this.handleVideoEnd.bind(this);
		this.updateCourseView = this.updateCourseView.bind(this);
	}
	componentDidMount(){
    this._isMounted = true;
	  axios
    .post('/api/courses/view', { slug: this.state.courseSlug }, { 'Content-Type': 'application/x-www-form-urlencoded' })
    .then(res => {
			if( res.data.success ) {
      	this.setState({course: res.data.course});
				store.getState().auth.user.accountStats.courses.map((d, i) => {
					if( d.slug === this.state.courseSlug ){
						this.setState({watchHistory: d.stages, loaded: true});
					}
					let w = 0, t = 0;
					d.stages.forEach(function(el, i){
					    el.levels.forEach(function(cl, j){
							t++;
							if( cl === 'watched') w++;
						});
					});
					this.setState({percentage: Math.round(w/t*100)});
				});
			}
    })
    .catch(err =>{
			return res.status(404).json({ error: err });
    });
	}

	componentWillUnmount(){
		this.GeneratedStage = '';
    this._isMounted = false;
	}
	simulateClick(e) { if(this._isMounted) e.click(); }
	componentDidUpdate(prevProps) {
		if(this.props.location.pathname !=  prevProps.location.pathname) {
			this.setState({stageSlug: this.props.match.params.stage});
		}
	}

	handleVideoEnd() {
		const el = document.querySelector('.video-list-item.current');
		el.classList.add('watched');
		if ( el.nextSibling != null ) el.nextSibling.click();
	}

	handleLevel(e, d, j, i) {
		this.setState({currentVideoUrl: d.levelVideoUrl});
		const el = document.querySelector('.video-list-item.current');
		if( el != null ) {
			el.classList.remove("current");
		}
		e.target.className += ' current';
		if( this.state.flag ) {
			this.updateCourseView(store.getState().auth.user.username, this.state.courseSlug, j, i);
		} else {
			this.setState({flag: true});
		}
	}

	updateCourseView(username, slug, j, i) {
		axios
		.post('/api/users/updateWatch', {
			username, slug, j, i
		}, { 'Content-Type': 'application/x-www-form-urlencoded' })
		.then(res => {
			this.GeneratedStage = this.generateStage();
		});
	}

	generateStage(){
		const stage = this.state.course.stages;
		let currentFlag = false,
				currentSrc;
		const Stage = stage.map( (el, j) => {
			return el.levels.map( (d, i) => {
				let w = this.state.watchHistory[j].levels[i];
				if ( w == 'unwatched' && currentFlag == false ) {
					w = 'current';
					currentSrc ="/src/assets/" + d.levelVideoUrl;
					currentFlag = true;
				}
				return (
					<div
						key={i}
						ref={(w == 'current') ? this.simulateClick :''}
						className={ "video-list-item " + w }
						onClick={ (e) => {this.handleLevel(e, d, j, i); ( j+1 == this.state.stageSlug ) ? {} : this.props.history.push('/course/'+this.state.courseSlug+'/'+(j+1)+'/')} }>
						<div className="video-list-status"><span></span></div>
						<div className="video-list-info">
							<div className="video-list-stage">
								<span className="video-list-stage-no">Stage {el.stageId}:</span>
								<span className="video-list-stage-name">{el.stageTitle}</span>
							</div>
							<div className="video-list-level">LEVEL {d.levelId}:</div>
							<div className="video-list-title">{d.levelTitle}</div>
						</div>
					</div>
				)
			});
		});
		return Stage;
	}

	render() {
		if ( !this.state.loaded ) {
			return (
				<div className="courseError text-center">
					Course not found
				</div>
			)
		} else {
			if (this.refs.player != undefined )	this.refs.player.actions.handleEnd = this.handleVideoEnd;
			this.GeneratedStage = this.generateStage();
			return (
				<section id="VideoPlayer">
					<Container>
						<Row>
							<Col md='12'>
								<div className="video-player-outer-wrapper">
									<div className="video-player-wrapper">
										<div className="video-player">
									    <Player
												id='Player'
											 	ref="player"
									      src={"/src/assets/" + this.state.currentVideoUrl}
									    />
										</div>
										<div className="video-list">
											{this.GeneratedStage}
										</div>
									</div>
									<div className="video-progress">
										<div className="video-progress-bar">
											<span style={{width: this.state.percentage  + '%'}}></span>
										</div>
										<div className="video-progress-text">COURSE PROGRESS: <span>{this.state.percentage}%</span></div>
									</div>
								</div>
							</Col>
						</Row>
					</Container>
				</section>
			)
		}
	}
}

export default withRouter(connect()(VideoPlayer));
