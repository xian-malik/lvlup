import React from 'react';

import Stages from '../Containers/SelectStages/Stages';

class SelectStage extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			courseSelection: ''
		}
	}
	render() {
		return (
	    <div className="page-content">
	      <Stages />
			</div>
		);
	}
}

export default SelectStage;
