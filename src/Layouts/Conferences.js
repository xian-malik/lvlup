import React from 'react';

import StayInformed from '../Containers/Conferences/StayInformed';

class Conferences extends React.Component {
	render() {
		return (
	    <div className="page-content">
				<StayInformed />
			</div>
		);
	}
}

export default Conferences;
