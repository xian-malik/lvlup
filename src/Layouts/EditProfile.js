import React from 'react';

import EditProfileForm from '../Containers/EditProfile/EditProfileForm';

class EditProfile extends React.Component {
	render() {
		return (
	    <div className="page-content">
				<EditProfileForm />
			</div>
		);
	}
}

export default EditProfile;
