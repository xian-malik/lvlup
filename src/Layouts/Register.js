import React from 'react';

import RegisterSection from '../Containers/Register/RegisterSection';

class Register extends React.Component {
	render() {
		return (
	    <div className="page-content">
	      <RegisterSection />
			</div>
		);
	}
}

export default Register;
