import React from 'react';

import VitalTips from '../Containers/FreeVideo/VitalTips';
import UpGame from '../Containers/Home/UpGame';

class FreeVideo extends React.Component {
	render() {
		return (
	    <div className="page-content">
				<VitalTips />
				<UpGame />
			</div>
		);
	}
}

export default FreeVideo;
