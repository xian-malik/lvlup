import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { hashHistory } from 'react-router';

import { Provider } from 'react-redux';
import store from './store';

import Header from "./components/common/Header";
import Footer from "./components/common/Footer";

import Home from "./Layouts/Home";
import Course from "./Layouts/Course";
import About from "./Layouts/AboutUs";
import Login from "./Layouts/Login";
import Register from "./Layouts/Register";
import Logout from "./Layouts/Logout";

import Coaching from "./Layouts/Coaching";
import EditProfile from "./Layouts/EditProfile";
import FreeVideo from "./Layouts/FreeVideo";
import MyAccount from "./Layouts/MyAccount";
import SelectStage from "./Layouts/SelectStage";
import VideoPlayer from "./Layouts/VideoPlayer";
import Conferences from "./Layouts/Conferences";

import './index.scss';
import './assets/fonts/fonts.scss';


function App() {
  return (
    <Provider store={ store }>
      <Router history={hashHistory}>
  			<Header logged={false} />
  			<Switch>
  	    	<Route path="/" exact component={Home} />
  	    	<Route path="/thecourse" component={Course} />
  	    	<Route path="/about" component={About} />
  				<Route path="/login" component={Login} />
  				<Route path="/register" component={Register} />

  				<Route path="/coaching" component={Coaching} />
  				<Route path="/editprofile" component={EditProfile} />
  				<Route path="/freevideo" component={FreeVideo} />
  				<Route path="/myaccount" component={MyAccount} />
  				<Route path="/selectstage" component={SelectStage} />
  				<Route path="/conferences" component={Conferences} />
          <Route path="/course/:course/:stage/" component={VideoPlayer} />
  				<Route path="/course/:course/" component={SelectStage} />

          <Route path="/logout" component={Logout} />
  			</Switch>
  			<Footer />
      </Router>
    </Provider>
  );
}

ReactDOM.render(<App/>, document.getElementById('root'));
