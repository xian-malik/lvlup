import React from 'react';
import Button from '../../components/common/Button';
import { Container, Row, Col } from '../../components/grid/Grid';

export default class Motivation extends React.Component {
	render() {
		return (
			<section id="OurMotivation">
				<Container>
					<Row>
						<Col md='12' data='text-center'>
							<h2 className="section-title no-margin">Our <span className="white-bg">Motivation</span></h2>
							<p className="small">WHY YOUR SUCCESS MATTERS TO US</p>
							<a href="#">
								<img src="./src/assets/images/aboutVideo.jpg" />
							</a>
							<Button text='BEGIN YOUR COURSE TODAY' link='/thecourse' />
						</Col>
					</Row>
				</Container>
			</section>
		)
	}
}
