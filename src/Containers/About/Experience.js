import React from 'react';
import Button from '../../components/common/Button';
import { Container, Row, Col } from '../../components/grid/Grid';

export default class Experience extends React.Component {
	render() {
		return (
			<section id="OurExperience">
				<Container>
					<Row>
						<Col md='12'>
							<h2 className="section-title text-center">
								OUR <span>EXPERIENCE</span>
							</h2>
						</Col>
						<Col md='6' offset='6'>
							<p className="small">You will be learning from a group of individuals that have
							all found major success on YouTube and Twitch.  Each of
							them have their own area of expertise and will be sharing
							all of their secrets with you.</p>
							<p><u>over <span className="number-big">5,000,000</span> subscribers</u></p>
							<p><u>over <span className="number-big">1,000,000,000</span> views</u></p>
							<p><u>over <span className="number-big">10,000</span> videos</u></p>
						</Col>
					</Row>
				</Container>
			</section>
		)
	}
}
