import React from 'react';
import Button from '../../components/common/Button';
import { Container, Row, Col } from '../../components/grid/Grid';

export default class Goal extends React.Component {
	render() {
		return (
      <section id="OurGoal">
				<Container>
					<Row>
						<Col md='12'>
							<h2 className="section-title">
								OUR <span>GOAL</span>
							</h2>
						</Col>
						<Col md='8' offset='2'>
							<p className="small">Helping you find success as a content creator!  This is our passion and primary focus. Thousands of hours of content are produced on YouTube and Twitch everyday but few people have the tools and knowledge for long term success.  We push to give you everything you need to grow your channel quickly and in the right way. </p>
						</Col>
						<Col md='3'>
							<img src="/src/assets/images/target.png" />
						</Col>
					</Row>
				</Container>
			</section>
		)
	}
}
