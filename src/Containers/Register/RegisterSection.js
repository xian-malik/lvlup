import React from 'react';
import { Link } from "react-router-dom";
import Button from '../../components/common/Button';
import { Container, Row, Col } from '../../components/grid/Grid';

import RegisterForm from '../../components/common/Forms/RegisterForm';

export default class RegisterSection extends React.Component {
	render() {
		return (
			<section id="LoginForm">
				<Container>
					<Row>
						<Col md='12'>
							<h2 className="section-title text-center">
								<span>REGISTER</span>
							</h2>
							<RegisterForm />
							<div className="text-center">
									Already have an account?
									<Link to="/login" className="navbar-link">
										<u>Login here</u>
									</Link>
							</div>
							<img src="./src/assets/images/couch.png" />
						</Col>
					</Row>
				</Container>
			</section>
		)
	}
}
