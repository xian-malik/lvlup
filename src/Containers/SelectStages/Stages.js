import React from 'react';
import { Link } from "react-router-dom";
import Button from '../../components/common/Button';
import { Container, Row, Col } from '../../components/grid/Grid';
import { withRouter } from 'react-router-dom';
import axios from 'axios';

import StageList from '../../components/Stages/StageList';

class Stages extends React.Component {
	constructor(props) {
		super(props);
		this.state =	{
			course: this.props.match.params.course,
			data: {},
			loaded: false,
		};
	}

	componentDidMount() {
		axios.post('/api/courses/view', { slug: this.state.course }, { 'Content-Type': 'application/x-www-form-urlencoded' })
		.then(res => {
			if( res.data.success ) {
				this.setState({data: res.data, loaded: true});
			}
		})
		.catch(err =>{
			console.log('axios error');
		});
	}

	render() {
		if (!this.state.loaded ) {
			return (
				<section id="Stages">
					<Container>
						<Row>
							<Col md='12'>
								<p>Loading</p>
							</Col>
						</Row>
					</Container>
				</section>
			)
		} else {
			return (
				<section id="Stages">
					<Container>
						<Row>
							<Col md='12'>
								<h2 className="section-title"><span>LEVEL UP</span> COURSE</h2>
								<StageList course={this.state.data.course}/>
							</Col>
						</Row>
					</Container>
				</section>
			)
		}
	}
}

export default withRouter(Stages);
