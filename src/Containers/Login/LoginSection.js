import React from 'react';
import { Link } from "react-router-dom";
import Button from '../../components/common/Button';
import { Container, Row, Col } from '../../components/grid/Grid';

import LoginForm from '../../components/common/Forms/LoginForm';

export default class LoginSection extends React.Component {

	render() {
		return (
			<section id="LoginForm">
				<Container>
					<Row>
						<Col md='12'>
							<h2 className="section-title text-center">
								LET’S <span>UP</span> YOUR CHANNEL <br/>
								TO THE NEXT <span>LEVEL!</span>
							</h2>
							<LoginForm />
							<div className="text-center">Need an account? <Link to="/register" className="navbar-link"><u>Register here</u></Link></div>
							<img src="./src/assets/images/couch.png" />
						</Col>
					</Row>
				</Container>
			</section>
		)
	}
}
