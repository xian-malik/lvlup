import React from 'react';
import { Player } from 'video-react';
import Button from '../../components/common/Button';
import { Container, Row, Col } from '../../components/grid/Grid';

export default class UpGame extends React.Component {
	render() {
		return (
			<section id="VitalTips">
				<Container>
					<Row>
						<Col md ='12'>
							<h2 className="section-title">
								Three <span className="white-bg">Vital</span> Tips
							</h2>
						</Col>
						<Col md='8'>
								<Player
						      playsInline
						      poster="./src/assets/images/freeVideoImage.png"
									src={"https://media.w3.org/2010/05/sintel/trailer_hd.mp4"}
								/>
						</Col>
						<Col md='4'>
							<h3 className="text-center"><span>The Course</span></h3>
							<p>
								<strong>3</strong> months of lessons <br/>
								<strong>Virality</strong> training <br/>
								<strong>Experienced</strong> Trainers <br/>
								Improved <strong>creation</strong> <br/>
								<strong>Overcome</strong> frustration <br/>
								Beat the <strong>algorithm</strong>
							</p>
							<div className="text-center">
								<p className="conference-price">$849</p>
								<Button text='START COURSE' />
							</div>
						</Col>
					</Row>
				</Container>
			</section>
		)
	}
}
