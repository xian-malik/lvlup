import React from 'react';
import Button from '../../components/common/Button';
import { Container, Row, Col } from '../../components/grid/Grid';

export default class StayInformed extends React.Component {
	render() {
		return (
			<section id="StayInformed">
				<Container>
					<Row>
						<Col md='12'>
							<h2 className="section-title text-center">
								<span className="white-bg">1 on 1</span> Coaching
							</h2>
						</Col>
					</Row>
					<Row type='center'>
						<Col md='8'>
							<img src="src/assets/images/couch.png" />
						</Col>
						<Col md='4'>
							<h3>THE <span className="blue-bg">NEXT</span> LEVEL</h3>
							<p>
								Each element and tool of the level up course
								that we give you needs to become second
								nature and a habbit.  This comes with time
								and can be hard to keep organized as you
								focus on creating your content.  With 1 on 1
								YouTube or Twitch coaching, you will receive
								instruction and direction specifically tailored
								to your channel for extreme success. John
								will meet with you monthly to review
								and study analytics, help you maintain the
								success mentality, and give additional tools
								to bring your channel to the next level at an
								expedited rate!
							</p>
							<div className="text-center">
								<p className="conference-price">$749/month</p>
								<Button text='START' />
							</div>
						</Col>
					</Row>
				</Container>
			</section>
		)
	}
}
