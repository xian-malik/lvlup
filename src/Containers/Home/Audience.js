import React from 'react';
import Button from '../../components/common/Button';
import { Container, Row, Col } from '../../components/grid/Grid';

export default class Audience extends React.Component {
	render() {
		return (
			<section id="Audience">
				<Container>
					<Row>
						<Col md='12'>
							<h2 className="section-title">Grow <span>your</span> audience</h2>
						</Col>
					</Row>
					<Row type='center'>
						<Col md="6">
							<div className="pd30">
									<p className="text-right">over <strong>60</strong> video lessons</p>
									<p className="text-right">become an <strong>algorithm</strong> pro</p>
									<p className="text-right">up your <strong>video/audio</strong> game</p>
									<p className="text-right">learn the success <strong>mentality</strong></p>
									<p className="text-right"><strong>grow</strong> with other creators</p>
									<p className="text-right">guidance from industry <strong>specialists</strong></p>
							</div>
						</Col>
						<Col md="6">
							<img src='src/assets/images/barChart.svg' />
						</Col>
					</Row>
				</ Container>
			</section>
		)
	}
}
