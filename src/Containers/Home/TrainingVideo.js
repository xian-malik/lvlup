import React from 'react';
import Button from '../../components/common/Button';
import { Container, Row, Col } from '../../components/grid/Grid';

export default class TrainingVideo extends React.Component {
	render() {
		return (
      <section id="TrainingVideo">
        <Container>
          <Row>
            <Col md='12'>
              <h2 className="section-title">Free Training <span className="white-bg">Video</span></h2>
            </Col>
            <Col md='10' offset='1' data='pd30'>
            	<img src="./src/assets/images/freeVideoImage.png" alt="Free Video" />
            </Col>
            <Col md='12' data='text-center'>
							<Button text="Watch The Video" link="/freevideo" />
            </Col>
          </Row>
        </Container>
      </section>
		)
	}
}
