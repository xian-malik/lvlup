import React from 'react';
import Button from '../../components/common/Button';
import { Container, Row, Col } from '../../components/grid/Grid';

export default class HomeSlider extends React.Component {
	render() {
		return (
			<section id="HomeSlider">
				<Container>
					<Row>
						<Col md="12">
							<h1 className="slider-text">LET'S <span>UP</span> YOUR CHANNEL<br/>TO THE NEXT <span>LEVEL!</span></h1>
							<Button
								text="START"
								type="slider-btn"
								link="/thecourse" />
							<div className="slider-image"><img src="./src/assets/images/couch.png" /></div>
						</Col>
					</Row>
				</Container>
			</section>
		)
	}
}
