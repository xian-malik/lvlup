import React from 'react';
import Button from '../../components/common/Button';
import { Container, Row, Col } from '../../components/grid/Grid';

export default class UpGame extends React.Component {
	render() {
		return (
			<section id="UpGame">
				<Container>
					<Row>
						<Col md='12'>
							<h2 className="section-title text-center">
								Up Your <span>Game</span>
							</h2>
						</Col>
						<Col md='5' offset='1'>
							<hr />
							<div className="pd30">
								<h3><span>Video</span> Conferences</h3>
								<p>
									YouTube and Twitch are always changing!
									By taking part in our video conferences
									two times a month, you will alway be
									informed on how take advantage of these
									changes instead of becoming a victim
									to them.  You will also be able to ask the
									experts how to overcome frustrations
									you are having with your channel.
								</p>
							</div>
							<hr />
							<div className="text-center">
								<Button text="Join Conferences" link="/conferences" />
							</div>
						</Col>
						<Col md='5'>
							<hr />
							<div className="pd30">
								<h3><span>1 on 1</span> Coaching</h3>
								<p>
									YouTube and Twitch are always changing!
									By taking part in our video conferences
									two times a month, you will alway be
									informed on how take advantage of these
									changes instead of becoming a victim
									to them.  You will also be able to ask the
									experts how to overcome frustrations
									you are having with your channel.
								</p>
							</div>
							<hr />
							<div className="text-center">
								<Button text="Apply For Coaching" link="/coaching" />
							</div>
						</Col>
					</Row>
				</Container>
			</section>
		)
	}
}
