import React from 'react';
import Button from '../../components/common/Button';
import { Container, Row, Col } from '../../components/grid/Grid';

import InventoryList from '../../components/Inventory/InventoryList';

export default class Inventory extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			data: [
				'Course',
				'Conference',
				'Discord',
				'Coaching',
				'Free Conference',
				'Free Coaching'
			]
		}
	}
	render() {
		return (
			<section id="Inventory">
				<Row>
					<Col md='12'>
						<h2 className="section-title text-center">
							MY <span>INVENTORY</span>
						</h2>
						<InventoryList
						 	data={this.state.data} />
					</Col>
				</Row>
			</section>
		)
	}
}
