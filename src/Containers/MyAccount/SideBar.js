import React from 'react';
import Button from '../../components/common/Button';
import Unlocks from '../../components/common/Unlocks/Unlocks';
import ProgressCircle from '../../components/common/ProgressCircle';
import { Container, Row, Col } from '../../components/grid/Grid';

export default class SideBar extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			unlocks: [{
				text: 'Course',
				locked: true
			},{
				text: 'Conference',
				locked: true
			},{
				text: 'Coaching',
				locked: true
			}]
		};
	}
	render() {
		const data = this.state;
		return (
			<section id="SideBar">
				<h2 className="section-title text-center"><span>UNLOCKS</span></h2>
				<Unlocks data={this.state.unlocks} />
			</section>
		)
	}
}
