import React from 'react';
import Button from '../../components/common/Button';
import ProgressCircle from '../../components/common/ProgressCircle';
import { Container, Row, Col } from '../../components/grid/Grid';

export default class YourGoals extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			data: this.props.data
		}
	}
	render() {
		const s = this.state.data.subscriberGoals;
		const v = this.state.data.viewGoals;
		return (
			<section id="YourGoals">
				<Row>
					<Col md='12'>
						<h2 className="section-title">YOUR <span>GOALS</span></h2>
						<div className="goal-selector">
							<a href="#" className="goal-single">
								<i className="fab fa-youtube"></i>
							</a>
							<a href="#" className="goal-single">
								<i className="fab fa-twitch"></i>
							</a>
						</div>
						<h3><span className="blue-bg"><strong>SUBSCRIBER</strong></span> GOALS</h3>
					</Col>
					<Col md='5' offset='1'>
						<ProgressCircle data={Math.round(s.current / s.monthly * 100)} />
					</Col>
					<Col md='6'>
						<ProgressCircle data={Math.round(s.current / s.yearly * 100)} />
					</Col>
					<Col md='4'>
						<div className="goals-count">
							<p className="no-margin"><strong>CURRENT</strong></p>
							<p className="no-margin">{("0000000" + s.current).slice(-7)}</p>
							<p className="no-margin">04.21.19</p>
						</div>
					</Col>
					<Col md='4'>
						<div className="goals-count">
							<p className="no-margin">MONTHLY GOAL</p>
							<p className="no-margin"><span>{("0000000" + s.monthly).slice(-7)}</span></p>
							<p className="no-margin">05.06.19</p>
						</div>
					</Col>
					<Col md='4'>
						<div className="goals-count">
							<p className="no-margin">YEARLY GOAL</p>
							<p className="no-margin"><span>{("0000000" + s.yearly).slice(-7)}</span></p>
							<p className="no-margin">01.06.20</p>
						</div>
					</Col>
				</Row>
				<Row>
					<Col md='12'>
						<h3><span className="blue-bg">VIEW</span> GOALS</h3>
					</Col>
					<Col md='5' offset='1'>
						<ProgressCircle data={Math.round(v.current / v.monthly * 100)} />
					</Col>
					<Col md='6'>
						<ProgressCircle data={Math.round(v.current / v.yearly * 100)} />
					</Col>
					<Col md='4'>
						<div className="goals-count">
							<p className="no-margin"><strong>CURRENT</strong></p>
							<p className="no-margin">{("0000000" + v.current).slice(-7)}</p>
							<p className="no-margin">04.21.19</p>
						</div>
					</Col>
					<Col md='4'>
						<div className="goals-count">
							<p className="no-margin">MONTHLY GOAL</p>
							<p className="no-margin"><span>{("0000000" + v.monthly).slice(-7)}</span></p>
							<p className="no-margin">05.06.19</p>
						</div>
					</Col>
					<Col md='4'>
						<div className="goals-count">
							<p className="no-margin">YEARLY GOAL</p>
							<p className="no-margin"><span>{("0000000" + v.yearly).slice(-7)}</span></p>
							<p className="no-margin">01.06.20</p>
						</div>
					</Col>
				</Row>
			</section>
		)
	}
}
