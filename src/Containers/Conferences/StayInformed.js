import React from 'react';
import Button from '../../components/common/Button';
import { Container, Row, Col } from '../../components/grid/Grid';

export default class StayInformed extends React.Component {
	render() {
		return (
			<section id="StayInformed">
				<Container>
					<Row>
						<Col md='12'>
							<h2 className="section-title text-center">
								<span className="white-bg">Video</span> Conferences
							</h2>
						</Col>
					</Row>
					<Row type='center'>
						<Col md='8'>
							<img src="./src/assets/images/conferenceImage.jpg" />
						</Col>
						<Col md='4'>
							<h3>Stay <span className="blue-bg">INFORMED</span></h3>
							<p>
								We know what it feels like when you are
								just starting your YouTube or Twitch channel.
								Everyone else knows the secrets and you
								have no idea how to work the algorithm or
								what it takes to make viral content.  By taking
								part in video conferences with John & Joe, you
								will begin to learn how.  Two times a month,
								you will have the opportunity to get your
								questions answered through live group chat
								and be instructed on current topics relevant
								to the YouTube and Twitch community.  If you
								are wanting to gain an advantage over the
								competition and grow your audience, this is
								essential for you.
							</p>
							<div className="text-center">
								<p className="conference-price">$99/month</p>
								<Button text='START' />
							</div>
						</Col>
					</Row>
				</Container>
			</section>
		)
	}
}
