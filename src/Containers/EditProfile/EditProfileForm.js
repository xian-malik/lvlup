import React from 'react';
import Button from '../../components/common/Button';
import { Container, Row, Col } from '../../components/grid/Grid';

import Unlocks from '../../components/common/Unlocks/Unlocks';
import AccountForm from '../../components/common/Forms/AccountForm';

export default class EditProfileForm extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			unlocks: [{
				text: 'Course',
				locked: true
			},{
				text: 'Conference',
				locked: true
			},{
				text: 'Coaching',
				locked: true
			}]
		};
	}
	render() {
		return (
			<section id="EditProfile">
				<Container>
					<Row>
						<Col md='8' data='text-center'>
							<h2 className="section-title"><span>ACCOUNT</span> DETAILS</h2>
							<p>profile picture</p>
							<AccountForm />
							<form action="#" id="EditPaymentForm" className="center-form edit-form">
								<div className="input-header">
									<p>PRIVATE ACCOUNT DETAILS</p>
								</div>
								<div className="input-group">
									<label htmlFor="fi-cardInfo">CARD INFO</label>
									<input type="text" name="fi-cardInfo" id="fi-cardInfo" value="" onChange={ () => {} } />
								</div>
								<div className="input-group">
									<label htmlFor="fi-billingAddress">BILLING ADDRESS</label>
									<input type="text" name="fi-billingAddress" id="fi-billingAddress" value="" onChange={ () => {} } />
								</div>
								<div className="input-group submit-save">
									<input type="submit" name="fi-submit" value="SAVE" />
								</div>
							</form>
						</Col>
						<Col md='4'>
							<h2 className="section-title text-center"><span>UNLOCKS</span></h2>
							<Unlocks data={this.state.unlocks} />
						</Col>
					</Row>
				</Container>
			</section>
		)
	}
}
