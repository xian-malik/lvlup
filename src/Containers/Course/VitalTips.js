import React from 'react';
import Button from '../../components/common/Button';
import { Container, Row, Col } from '../../components/grid/Grid';

export default class VitalTips extends React.Component {
	render() {
		return (
			<section id="VitalTips">
				<Container>
					<Row>
						<Col md='12'>
							<h2 className="section-title">
								WHAT’S <span className="white-bg">INSIDE?</span>
							</h2>
						</Col>
						<Col md='8'>
							<div className="video-image">
								<a href="#">
									<img src="./src/assets/images/promoVideo.png" />
								</a>
							</div>
						</Col>
						<Col md='4'>
							<h3 className="text-center"><span>INCLUDES</span></h3>
							<p>
								<strong>3</strong> months of lessons <br/>
								<strong>Virality</strong> training <br/>
								<strong>Experienced</strong> Trainers <br/>
								Improved <strong>creation</strong> <br/>
								<strong>Overcome</strong> frustration <br/>
								Beat the <strong>algorithm</strong>
							</p>
              <div className="text-center">
								<p className="conference-price">$99/year</p>
								<Button text='START COURSE' />
							</div>
						</Col>
					</Row>
				</Container>
			</section>
		)
	}
}
