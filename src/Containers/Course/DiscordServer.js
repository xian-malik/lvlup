import React from 'react';
import Button from '../../components/common/Button';
import Carousel from '../../components/common/Carousel';
import { Container, Row, Col } from '../../components/grid/Grid';

export default class DiscordServer extends React.Component {
	render() {
		return (
			<section id="DiscordServer">
        <Container>
          <Row type='bottom flex-end'>
            <Col md='6'>
							<img src="./src/assets/images/discord.png" alt="Discord Server" />
      			</Col>
						<Col md='6' data='pd5p'>
							<h2 className="section-title">
								CREATORS<br/><span>DISCORD</span> SERVER
							</h2>
							<p>
								<strong>Networking</strong> and having a community of like minded people around you is <strong>vitally</strong> important.  We will help you form that <strong>community</strong> by providing every course member with access to a private discord where you can share your journey and find friends to connect and <strong>grow</strong> with.
							</p>
						</Col>
      		</Row>
      	</Container>
      </section>
		)
	}
}
