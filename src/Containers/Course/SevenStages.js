import React from 'react';
import Button from '../../components/common/Button';
import Carousel from '../../components/common/Carousel';
import { Container, Row, Col } from '../../components/grid/Grid';

export default class SevenStages extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			stagesData: [
				{
					head: 'MENTAL PREPERATION',
					text: 'You can have all of the tools in the world to create great YouTube videos or produce a great stream, but nothing is more important than having the mental toughness and drive to push through frustration.  We will help you overcome that and prepare you for success.'
				},
				{
					head: 'MENTAL PREPERATION',
					text: 'You can have all of the tools in the world to create great YouTube videos or produce a great stream, but nothing is more important than having the mental toughness and drive to push through frustration.  We will help you overcome that and prepare you for success.'
				},
				{
					head: 'MENTAL PREPERATION',
					text: 'You can have all of the tools in the world to create great YouTube videos or produce a great stream, but nothing is more important than having the mental toughness and drive to push through frustration.  We will help you overcome that and prepare you for success.'
				},
				{
					head: 'MENTAL PREPERATION',
					text: 'You can have all of the tools in the world to create great YouTube videos or produce a great stream, but nothing is more important than having the mental toughness and drive to push through frustration.  We will help you overcome that and prepare you for success.'
				},
				{
					head: 'MENTAL PREPERATION',
					text: 'You can have all of the tools in the world to create great YouTube videos or produce a great stream, but nothing is more important than having the mental toughness and drive to push through frustration.  We will help you overcome that and prepare you for success.'
				},
				{
					head: 'MENTAL PREPERATION',
					text: 'You can have all of the tools in the world to create great YouTube videos or produce a great stream, but nothing is more important than having the mental toughness and drive to push through frustration.  We will help you overcome that and prepare you for success.'
				},
				{
					head: 'MENTAL PREPERATION',
					text: 'You can have all of the tools in the world to create great YouTube videos or produce a great stream, but nothing is more important than having the mental toughness and drive to push through frustration.  We will help you overcome that and prepare you for success.'
				},
			]
		};
	}

	render() {
		return (
      <section id="SevenStages">
        <Container>
          <Row>
            <Col md='12'>
              <h2 className="section-title">
                THE 7 <span>STAGES</span>
              </h2>
							<Carousel data={this.state.stagesData} name="stages" />
            </Col>
          </Row>
        </Container>
      </section>


		)
	}
}
