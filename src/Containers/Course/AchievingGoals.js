import React from 'react';
import Button from '../../components/common/Button';
import Carousel from '../../components/common/Carousel';
import { Container, Row, Col } from '../../components/grid/Grid';

export default class AchievingGoals extends React.Component {
	render() {
		return (
			<section id="AchievingGoals">
        <Container>
          <Row>
            <Col md='12'>
							<h2 className="section-title text-center">
								ACHIEVING YOUR <span>GOALS</span>
							</h2>
            </Col>
            <Col md='6'>
							<h3><span className="blue-bg">SUBSCRIBER</span> GOALS</h3>
							<img src="./src/assets/images/subGoals.png" alt="" />
            </Col>
            <Col md='6'>
							<p>
								We believe that setting <strong>goals</strong> for <br/>
								yourself is a <strong>vital</strong> step in your path to <br/>
								success. We help you track your <strong>growth</strong> <br/>
								and progress by setting realistic, achievable <br/>
								goals that you can <strong>track</strong> regularly.
							</p>
						</Col>
            <Col md='12' data='text-center'>
							<Button text='CREATE ACCOUNT' link='/register' />
            </Col>
          </Row>
        </Container>
			</section>
		)
	}
}
