import path from 'path';
import express from 'express';
import webpack from 'webpack';
import mongoose from 'mongoose';
import passport from 'passport';
import bodyParser from 'body-parser';

import KEYS from './config/keys';
import config from './webpack.config.js';

import users from './api/routes/users';
import courses from './api/routes/courses';

const compiler = webpack(config);

const app = express(),
      DIST_DIR = __dirname,
      HTML_FILE = path.join(DIST_DIR, '/index.html');

const webpackDevMiddleware = require('webpack-dev-middleware')(
  compiler,
  config.devServer
);

// Passport Middleware
app.use(passport.initialize());
// Passport initialize
require('./config/passport')(passport);

const webpackHotMiddleware = require('webpack-hot-middleware')(compiler);

app.use(webpackDevMiddleware);
app.use(webpackHotMiddleware);

const staticMiddleware = express.static('dist');

app.use(staticMiddleware);

// Body Parser Middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Database Config
const db = KEYS.mongoURI;

// Database INIT
mongoose
  .connect(db, { useNewUrlParser: true })
  .then(() => console.log('MongoDB Connected'))
  .catch(err => console.log(err));

app.use(express.static(DIST_DIR));

// Use Routes
app.use('/api/users', users);
app.use('/api/courses', courses);
app.get('*', (req, res) => { res.sendFile(HTML_FILE) });


app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
    console.log(`App listening to ${PORT}....`)
});
