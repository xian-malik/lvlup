import mongoose from 'mongoose';
const Schema = mongoose.Schema;

// Create Schema
const UserSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  username: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  dob: {
    type: Date,
    default: Date.now
  },
  countrycode: {
    type: String,
    required: true
  },
  phone: {
    type: String,
    required: true
  },
  accountStats: new Schema({
    _id: false,
    courses: [new Schema({
      _id: false,
      slug: { type: String },
      stages: [new Schema({
        _id: false,
        levels: [{type: String }]
      })]
    })]
  })
}, {
  collection: 'users'
});
const User = mongoose.model('users', UserSchema);
export default User;
