import mongoose from 'mongoose';
const Schema = mongoose.Schema;

export const LevelSchema = new Schema({
  _id: false,
  levelId: {
    type: Number,
    required: true
  },
  levelTitle: {
    type: String,
    required: true
  },
  levelVideoUrl: {
    type: String,
    required: true
  }
});
export const Level = mongoose.model('level', LevelSchema);

const StageSchema = new Schema({
  _id: false,
  stageId: {
    type: Number,
    required: true
  },
  stageTitle: {
    type: String,
    required: true
  },
  levels: [LevelSchema]
});
export const Stage = mongoose.model('stage', StageSchema);
// Create Schema
const CourseSchema = new Schema({
  slug: {
    type: String,
    required: true
  },
  title: {
    type: String,
    required: true
  },
  stages: [StageSchema]
}, {
  collection: 'courses'
});
const Course = mongoose.model('courses', CourseSchema);
export default Course;
