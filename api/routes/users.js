import express from 'express';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import passport from 'passport';
import keys from '../../config/keys';

// Load User model
import User from '../schema/User';

const router = express.Router();

// @route   GET api/users/test
// @desc    Tests users route
// @access  Public
router.get('/test', (req, res) => res.json({ msg: 'User API is working.' }));

// @route   GET api/users/register
// @desc    Register New Users
// @access  Public
router.post('/register', (req, res) => {
  User.findOne({ username: req.body.username })
    .then(user => {
      if ( user ) {
        return res.status(400).json({username: 'Username already exists'})
      } else {
        const newUser = new User({
          name: req.body.name,
          username: req.body.username,
          email: req.body.email,
          password: req.body.password,
          accountStats: {}
        });

        bcrypt.genSalt(10, (err, salt) => {
          bcrypt.hash(newUser.password, salt, (err, hash) => {
              if (err) throw err;
              newUser.password = hash;
              newUser.save()
                .then(user => res.json(user))
                .catch(err => console.log(err));
          })
        })
      }
    })
});

// @route   GET api/users/register
// @desc    Register New Users
// @access  Public
router.post('/edit', (req, res) => {
  let user = {};
  if( req.body.name != null ) {
    user.name = req.body.name;
  }
  if( req.body.email != null ) {
    user.email = req.body.email;
  }
  if( req.body.countrycode != null ) {
    user.countrycode = req.body.countrycode;
  }
  if( req.body.phone != null ) {
    user.phone = req.body.phone;
  }

  if( req.body.dob != null ) {
    user.dob = req.body.dob;
  }

  if( req.body.password != null ) {
    bcrypt.genSalt(10, (err, salt) => {
      bcrypt.hash(req.body.password, salt, (err, hash) => {
        if (err) throw err;
        user.password = hash;
        User.findOneAndUpdate(
          { username: req.body.username },
          { $set: user }
        )
        .then(u => {
          res.json({success: 'ok', user: u});
        });
      });
    });
  } else {
    User.findOneAndUpdate(
      { username: req.body.username },
      { $set: user }
    )
    .then(u => {
      res.json({success: 'ok', user: u});
    });
  }


});

// @route   GET api/users/login
// @desc    Login User / Returning JWToken
// @access  Public
router.post('/login', (req, res) => {
  const username = req.body.username;
  const password = req.body.password;
  User.findOne({ username })
    .then(user => {
      // Check user
      if(!user)
        return res.status(404).json({ username: 'User not found' });

      // Check password
      bcrypt.compare(password, user.password)
        .then( isMatch => {
          if(isMatch) {
            // Sign Token
            const payload = {
              id: user.id,
              name: user.name,
              username: user.username,
              email: user.email,
              password: user.password,
              dob: user.dob,
              countrycode: user.countrycode,
              phone: user.phone,
              accountStats: user.accountStats
            };
            jwt.sign(
              payload,
              keys.token,
              { expiresIn: 3600 },
              (err, token ) => {
                res.json({
                  success: true,
                  token: 'Bearer ' + token,
                })
              }
            );
          } else {
            return res.status(400).json({password: 'Password Incorrect'})
          }
        })
        .catch(err => {

        });
    });
  }
);


// @route   GET api/users/current
// @desc    Return Current User
// @access  Private
router.get(
  '/current',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    User.findOne({ username: req.body.username })
      .then(user => {
        if(!user) {
          return res.status(404).json({ username: 'User not found' });
        } else {
          return res.status(200).json({ user: user });
        }
      });
  }
);


// @route   POST api/users/updateWatch
// @desc    Updates course watch list
// @access  Private
router.post(
  '/updateWatch',
  (req, res) => {
    let set = {};
    set['accountStats.courses.$.stages.' + req.body.j + '.levels.' + req.body.i + ''] = 'watched';
    User.findOneAndUpdate({
      username: req.body.username,
      'accountStats.courses.slug': req.body.slug
    }, {
      $set: set
    })
    .then(user => {
      if(!user) {
        return res.status(404).json({ error: user });
      } else {
        return res.status(200).json({ user: user });
      }
    }).catch(err => {
      return res.status(404).json({error: err});
    });
  }
);

const users = router;
export default users;
