import express from 'express';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import passport from 'passport';
import keys from '../../config/keys';

// Load Course model
import Course, { Stage, Level } from '../schema/Course';

const router = express.Router();

// @route   GET api/courses/test
// @desc    Tests courses route
// @access  Public
router.get('/test', (req, res) => res.json({ msg: 'Course API is working.' }));

// @route   GET api/courses/register
// @desc    Register New Courses
// @access  Public
router.post('/register', (req, res) => {
  Course.findOne({ slug: req.body.slug })
    .then(course => {
      if ( course ) {
        return res.status(400).json({slug: 'Course name already exists'})
      } else {
        let newLevel = new Level({
          levelId: req.body.levelId,
          levelTitle: req.body.levelTitle,
        });
        let newStage = new Stage({
          stageId: req.body.stageId,
          stageTitle: req.body.stageTitle,
          levels: [newLevel]
        });
        let newCourse = new Course({
          slug: req.body.slug,
          title: req.body.title,
          videoLink: req.body.videoLink,
          stages: [newStage]
        });
        newCourse.save()
          .then(course => res.json(course))
          .catch(err => console.log(err));
        }
    })
});

// @route   GET api/courses/login
// @desc    Login Course / Returning JWToken
// @access  Public
router.post('/view', (req, res) => {
    const slug = req.body.slug;
    Course.findOne({ slug })
      .then(course => {
        // Check course
        if(!course) {
          return res.status(404).json({ course: 'Course not found' });
        } else {
          res.json({
            success: true,
            course: course
          })
        }
      });
  }
);

const courses = router;
export default courses;
