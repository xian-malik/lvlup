const path = require('path');
const webpack = require('webpack');
const HWP = require('html-webpack-plugin');

module.exports = {
  mode: 'development',
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'build.js',
    publicPath: './'
  },
  devServer: {
    contentBase: 'dist',
    overlay: true,
    hot: true,
    stats: {
      colors: true,
    },
    historyApiFallback: true
  },
  module:	{
    rules:[{
      test: /\.js$/,
      exclude: /node_modules/,
      loader: 'babel-loader',
      query: {
           presets: ['react', 'es2015', 'stage-3']
      },
    },{
  		test: /\.(?:sa|sc|c)ss$/,
      use: [ "style-loader", "css-loader", "sass-loader", ]
    },{
			test: /\.(jpe?g|png|gif|svg)$/i,
			use: [{
				loader: 'url-loader',
				options: {
					limit: 8000,
					name: '[name].[ext]',
          outputPath: "/assets/images/"
				}
			}]
		},{
			test: /.(ttf|otf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
      use: {
        loader: "file-loader",
        options: { outputPath: "fonts/" }
      }
    }]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new HWP({ template: './index.html' })
  ]
}
